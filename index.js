const apiKey = "6d2797323e56ca48eb642ab0b755c345";
const apiToken = `ATTAad08478215919a13ac9be82d6f3f8402de8f23ba3abfea9f91c4aed038231073D59F288C`;

function getBoard(boardId) {
  return new Promise((resolve, reject) => {
    const getDetails = fetch(
      `https://api.trello.com/1/boards/${boardId}?key=${apiKey}&token=${apiToken}`
    );
    getDetails
      .then((result) => {
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function createBoard(boardName) {
  return new Promise((resolve, reject) => {
    const newBoard = fetch(
      `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`,
      {
        method: "POST",
      }
    );
    newBoard
      .then((result) => {
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function getLists(boardId) {
  return new Promise((resolve, reject) => {
    const getList = fetch(
      `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    );
    getList
      .then((result) => {
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function getCards(listId) {
  return new Promise((resolve, reject) => {
    const cardData = fetch(
      `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      }
    );

    cardData
      .then((result) => {
        resolve(result);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function getAllCards(boardId) {
  return new Promise((resolve, reject) => {
    const getList = getLists(boardId);
    getList
      .then((result) => {
        return result.json();
      })
      .then((result) => {
        const array = [];
        result.map((current_list) => {
          array.push(current_list.id);
        });
        return array;
      })
      .then((result) => {
        let store_promise = [];
        for (let index = 0; index < result.length; index++) {
          store_promise.push(getCards(result[index]));
        }
        resolve(store_promise);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function createBoardListAndCards() {
  return new Promise((resolve, reject) => {
    const newBoard = createBoard("Try Again");
    newBoard
      .then((result) => {
        return result.json();
      })
      .then((result) => {
        const getID = result.id;
        let count = 1;
        while (count <= 3) {
          const name = `Made a new List - ${count}`;
          const currentList = fetch(
            `https://api.trello.com/1/lists?name=${name}&idBoard=${getID}&key=${apiKey}&token=${apiToken}`,
            {
              method: "POST",
            }
          );

          currentList
            .then((result) => {
              return result.json();
            })
            .then((result) => {
              const listID = result.id;
              const cardName = "just";
              const createCard = fetch(
                `https://api.trello.com/1/cards?idList=${listID}&name=${cardName}&key=${apiKey}&token=${apiToken}`,
                {
                  method: "POST",
                  headers: {
                    Accept: "application/json",
                  },
                }
              );
              createCard.then(() => {
                console.log("Card is created");
              });
            })
            .catch((error) => {
              reject(error);
            });

          count += 1;
        }
        resolve(getID);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function deleteAllLists() {
  return new Promise((resolve, reject) => {
    const previous_result = createBoardListAndCards();
    previous_result.then((result) => {
      const getList = getLists(result);
      getList
        .then((result) => {
          return result.json();
        })
        .then((result) => {
          let lists_ids = [];
          for (let index = 0; index < result.length; index++) {
            const id = result[index].id;
            console.log(id);
            fetch(
              `https://api.trello.com/1/lists/${id}/closed?value=true&key=${apiKey}&token=${apiToken}`,
              {
                method: "PUT",
              }
            );
          }
        });
    });
  });
}

function updateCheckListItems(cardID, checkListID) {
  return new Promise((resolve, reject) => {
    const check = fetch(
      `https://api.trello.com/1/checklists/${checkListID}/checkItems?key=${apiKey}&token=${apiToken}`,
      {
        method: "GET",
      }
    );
    check
      .then((result) => {
        return result.json();
      })
      .then((result) => {
        for (let item of result) {
          fetch(
            `https://api.trello.com/1/cards/${cardID}/checkItem/${item.id}?key=${apiKey}&token=${apiToken}&state=complete`,
            {
              method: "PUT",
            }
          );
        }
        resolve("All the items are checked successfully!!!");
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function updateCheckListItemsInComplete(cardID, checkListID) {
  return new Promise((resolve, reject) => {
    const check = fetch(
      `https://api.trello.com/1/checklists/${checkListID}/checkItems?key=${apiKey}&token=${apiToken}`,
      {
        method: "GET",
      }
    );
    check
      .then((result) => {
        return result.json();
      })
      .then((result) => {
        for (let item of result) {
          const now = fetch(
            `https://api.trello.com/1/cards/${cardID}/checkItem/${item.id}?key=${apiKey}&token=${apiToken}&state=incomplete`,
            {
              method: "PUT",
            }
          );
          now.then(() => {
            console.log("Check item is undo successfully!!!");
          });
        }
        resolve("All the items are undo successfully!!!");
      })
      .catch((error) => {
        reject(error);
      });
  });
}

module.exports = {
  getCards,
  getBoard,
  getLists,
  createBoard,
  getAllCards,
  createBoardListAndCards,
  deleteAllLists,
  updateCheckListItems,
  updateCheckListItemsInComplete,
};
