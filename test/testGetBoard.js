const {
  getCards,
  getBoard,
  getLists,
  createBoard,
  getAllCards,
  createBoardListAndCards,
  deleteAllLists,
} = require("/home/kcbiradar/Desktop/MountBlue/trello-promise/index.js");

getBoard("66540d56ea5222480a2ea396")
  .then((result) => {
    return result.json();
  })
  .then((result) => {
    console.log(result);
  })
  .catch((error) => {
    console.error(error.message);
  });
