const {
  getCards,
  getBoard,
  getLists,
  createBoard,
  getAllCards,
  createBoardListAndCards,
  deleteAllLists,
} = require("/home/kcbiradar/Desktop/MountBlue/trello-promise/index.js");

createBoard("wait")
  .then((result) => {
    return result.json();
  })
  .then((result) => {
    console.log(result.id);
  })
  .catch((error) => {
    console.log(error);
  });
