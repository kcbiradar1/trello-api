const {
  getCards,
  getBoard,
  getLists,
  createBoard,
  getAllCards,
  createBoardListAndCards,
  deleteAllLists,
} = require("/home/kcbiradar/Desktop/MountBlue/trello-promise/index.js");

createBoardListAndCards().then((result) => {
  console.log("Board created with board ID ", result);
});
