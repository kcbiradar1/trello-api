const {
  getCards,
  getBoard,
  getLists,
  createBoard,
  getAllCards,
  createBoardListAndCards,
  deleteAllLists,
  updateCheckListItems,
  updateCheckListItemsInComplete,
} = require("/home/kcbiradar/Desktop/MountBlue/trello-promise/index.js");

updateCheckListItemsInComplete(
  "665405093809ee067e41a3e0",
  "66549ae39e71e99c40dacfff"
).then((res) => {
    console.log(res);
})
.catch((error) => {
    console.log(error);
})
